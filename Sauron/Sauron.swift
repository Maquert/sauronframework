//
//  Sauron.swift
//  Sauron
//
//  Created by Miguel Hernández Jaso on 24/11/14.
//  Copyright (c) 2014 Miguel Hernández Jaso. All rights reserved.
//

import Foundation
import UIKit



public class Sauron
{
    
    // MARK: Switch
    
    public class func switchToStoryboard(name: String, identifier: String?,
        completion:( (nextController: UIViewController) -> Void)? ) -> UIViewController?
    {
        if let nextController: UIViewController = retrieveNextViewController(fromStoryboard: name, identifier: identifier)
        {
            if let aCompletionClasure = completion {
                aCompletionClasure(nextController: nextController)
            }
            
            setRootWithController(nextController)
            return nextController
        }
        
        return nil
    }
    
    
    // MARK: Interrupt
    
    public class func interruptWithStoryboard(name: String, identifier: String,
        completion:( (nextController: UIViewController) -> Void)? ) -> UIViewController?
    {
        if let nextController:UIViewController = retrieveNextViewController(fromStoryboard: name,
            identifier: identifier)
        {
            if let rootViewController = appRootVC() {
                
                if let aCompletionClasure = completion {
                    aCompletionClasure(nextController: nextController)
                }
                
                rootViewController.presentViewController(nextController, animated: true, completion: nil)
                return nextController
            }
        }
        return nil
    }
    
    
    
    
    // MARK: - Instances
    
    internal class func retrieveNextViewController(fromStoryboard name: String, identifier: String?) -> UIViewController?
    {
        var storyboard: UIStoryboard? = nil
        TryBlock.try( { () -> Void in
            storyboard = UIStoryboard(name: name, bundle: nil)
            }, catch: { (exception) -> Void in
                storyboard = nil
                println("[Sauron] ERROR: '\(name)' is not a valid storyboard.")
            }, finally: nil)
        
        if let aValidStoryboard = storyboard {
            var optionalController = instantiateStoryboard(aValidStoryboard, identifier: identifier)
            if let nextViewController = optionalController {
                return pushableVC(nextViewController)
            }
        }
        return nil
    }
    
    
    // MARK: Storyboards and ViewControllers
    
    class private func storyboardNamed(name: String) -> UIStoryboard
    {
        let storyboard =  UIStoryboard(name: name, bundle: nil)
        return storyboard
    }
    
    internal class func instantiateStoryboard(storyboard: UIStoryboard, identifier: String?) -> UIViewController?
    {
        if let anIdentifier = identifier
        {
            return storyboard.instantiateViewControllerWithIdentifier(anIdentifier) as? UIViewController
        }
        else {
            return storyboard.instantiateInitialViewController() as? UIViewController
        }
    }
    
    class internal func pushableVC(controller: UIViewController) -> UIViewController?
    {
        if let navigationController = controller as? UINavigationController
        {
            return navigationController.viewControllers.first as? UIViewController
        }
        
        return controller
        
    }
    
    
    
    
    
    // MARK: Handy Methods
    
    class private func navigationController(forViewController controller: UIViewController) -> UINavigationController
    {
        if let navVC = controller.navigationController {
            return navVC
        }
        else {
            return UINavigationController(rootViewController: controller)
        }
    }
    
    class func appRootVC() -> UIViewController?
    {
        let app = UIApplication.sharedApplication()
        if let rootVC = app.delegate?.window??.rootViewController
        {
            return rootVC
        }
        return nil
    }
    
    
    class private func setRootWithController(controller: UIViewController)
    {
        if let appWindow = UIApplication.sharedApplication().delegate?.window
        {
            appWindow?.rootViewController = controller
        }
    }
    
}



