//
//  UIViewControllerExtension.swift
//  Sauron
//
//  Created by Miguel Hernández Jaso on 24/11/14.
//  Copyright (c) 2014 Miguel Hernández Jaso. All rights reserved.
//

import Foundation
import UIKit

public extension UIViewController
{
    
    
    // MARK: Push
    
    public func pushToStoryboard(named: String, identifier: String?,
        completion:( (nextController: UIViewController) -> Void)? ) -> UIViewController?
    {
        if let nextController = Sauron.retrieveNextViewController(fromStoryboard: named, identifier: identifier)
        {
            if let aCompletionClasure = completion {
                aCompletionClasure(nextController: nextController)
            }
            
            if let currentNavigationController = self.navigationController {
                currentNavigationController.pushViewController(nextController, animated: true)
                return nextController
            }
            else {
                println("[Sauron] ERROR: No navigation controller found.")
            }
            
        }
        
        return nil
    }
    
    
    
    // MARK: Present
    
    public func presentStoryboard(named: String, identifier: String,
        completion:( (nextController: UIViewController) -> Void)? ) -> UIViewController?
    {
        if let nextController = Sauron.retrieveNextViewController(fromStoryboard: named, identifier: identifier)
        {
            if let aCompletionClasure = completion {
                aCompletionClasure(nextController: nextController)
            }
            
            presentViewController(nextController, animated: true, completion: nil)
            return nextController
        }
        
        return nil
    }
    
    
    // MARK: Hook
    // Inserts a NavigationController before the presented ViewController
    public func hookStoryboard(named: String, identifier: String,
        completion:( (nextController: UIViewController) -> Void)? )
    {
        if let nextController = Sauron.retrieveNextViewController(fromStoryboard: named, identifier: identifier)
        {
            if let aCompletionClasure = completion {
                aCompletionClasure(nextController: nextController)
            }
            
            let navigationController = UINavigationController(rootViewController: nextController)
            presentViewController(navigationController, animated: true, completion: nil)
        }
        
    }
    
    
    
    
    
}

