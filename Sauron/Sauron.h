//
//  Sauron.h
//  Sauron
//
//  Created by Miguel Hernández Jaso on 1/27/15.
//  Copyright (c) 2015 Miguel Hernández Jaso. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Sauron.
FOUNDATION_EXPORT double SauronVersionNumber;

//! Project version string for Sauron.
FOUNDATION_EXPORT const unsigned char SauronVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Sauron/PublicHeader.h>

#import <Sauron/TryBlock.h>
